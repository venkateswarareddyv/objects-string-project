function mapObject(obj,callBackForProblem3) {
    if(typeof obj === "object"){
        let  newObject={};
        for (let keys in obj){
            let newValue=callBackForProblem3(obj[keys]);
            newObject[keys]=newValue;
        }
        return newObject;
    }
}

module.exports =mapObject;
