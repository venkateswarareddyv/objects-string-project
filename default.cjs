function defaults(obj, defaultProps){
    if (typeof obj ==="object"){
        for(let item in defaultProps){
            if(item in obj ===false){
                obj[item]=defaultProps[item];
            }
        }
    }
    return obj;
}


module.exports=defaults;