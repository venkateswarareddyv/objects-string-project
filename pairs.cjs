function pairs(obj) {
    if(typeof obj ==="object"){
        let newObject=[];
        for (let keys in obj){
            newObject.push([keys , obj[keys]]);
        }
        return newObject;
    }
}

module.exports=pairs;
